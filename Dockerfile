FROM debian:stretch

RUN apt-get update && \
    apt-get install -y git curl wget make gcc bc libncurses-dev && \
    git clone --recursive https://github.com/TiejunChina/linux.git && \
    git clone --recursive https://github.com/raspberrypi/tools.git && \
    mkdir rtkernel

ENV ARCH=arm \
    PATH=$PATH:/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin \
    CROSS_COMPILE=arm-linux-gnueabihf- \
    INSTALL_MOD_PATH=/rtkernel \
    KERNEL=kernel7

RUN apt-get install -y dpkg-dev

COPY rpi23b-rt.config rpi23b-rt.config
